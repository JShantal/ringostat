# Ringostat test task

## Task description
It is necessary to implement the service for outputting of the Twitter feed with the update in real time.

Requirements:
- composer is required.
- implementation in "clean" PHP.
- no more than 25 recent messages in output.
- jQuery, Angular, React or Vue for frontend.
- The architecture should allow the connection of other news feeds.

## Installation
```bash
composer install
npm install
npm run prod
```
