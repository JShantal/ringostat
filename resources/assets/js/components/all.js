Vue.component('tweets', require('./Tweets.vue'));
Vue.component('config', require('./ConfigModal.vue'));
Vue.component('users', require('./Users.vue'));
Vue.component('app-form', require('./AppForm.vue'));