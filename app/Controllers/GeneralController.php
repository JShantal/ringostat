<?php

namespace App\Controllers;

use App\Lib\View;

class GeneralController
{
    public function showIndexAction()
    {
        return View::show('index');
    }
}