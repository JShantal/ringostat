<?php

namespace App\Controllers;

use App\Lib\Parameter;
use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterController
{
    public function showAction()
    {
        $users = Parameter::get('twitter_users');
        $tweets = $this->getTweets($users);

        $result = [];
        foreach ($tweets as $tweet) {
            $data['id'] = $tweet->id_str;
            $data['text'] = $tweet->text;
            $data['media'] = $tweet->entities->media[0]->media_url_https ?? null;
            $data['user']['name'] = $tweet->user->name;
            $data['user']['screen_name'] = $tweet->user->screen_name;
            $data['user']['image'] = $tweet->user->profile_image_url_https;
            $result[$tweet->id] = $data;
        }
        krsort($result);
        $result = array_slice($result, 0, 25);

        return $result;
    }

    /**
     * Open basic connection to twitter app
     *
     * @return TwitterOAuth
     */
    private function getConnection()
    {
        $parameters = Parameter::get('twitter_app');
        return new TwitterOAuth($parameters['key'], $parameters['secret']);
    }

    /**
     * Get twitter users timelines
     *
     * @param array $users
     *
     * @return array
     */
    private function getTweets($users)
    {
        $connection = $this->getConnection();
        $tweets = [];
        foreach ($users as $user) {
            $data = $connection->get("statuses/user_timeline",
                ["count" => 25, "exclude_replies" => true, "screen_name" => $user]
            );
            if (is_array($data))
                $tweets = array_merge($data, $tweets);
        }

        return $tweets;
    }
}