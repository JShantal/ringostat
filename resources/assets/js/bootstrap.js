import Vue from 'vue';
import axios from 'axios';
import BootstrapVue from 'bootstrap-vue';

window.Vue = Vue;
Vue.use(BootstrapVue);

window.axios = axios;
/*
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};*/
Vue.prototype.$http = axios;