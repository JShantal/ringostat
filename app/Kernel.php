<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Kernel
{
    /**
     * Handle Request to convert it to a Response.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function handle(Request $request)
    {
        $routes = $this->getRoutes();
        $context = new Routing\RequestContext();
        $context->fromRequest($request);
        $matcher = new Routing\Matcher\UrlMatcher($routes, $context);
        try {
            $attributes = $matcher->matchRequest($request);
            $action = 'App\Controllers\\' . $attributes['action'];
            list($class, $method) = explode('@', $action);
            $class = new $class();
            unset($attributes['action']);
            if ($request->getMethod() == 'POST' || $request->getMethod() == 'PATCH')
                array_unshift($attributes, $request);
            $content = call_user_func_array([$class, $method], $attributes);
            if ($content instanceof Response)
                $response = $content;
            elseif (is_null($content))
                return new Response();
            else
                $response = new JsonResponse($content);
        } catch (ResourceNotFoundException $e) {
            $response = new Response('Not found!', Response::HTTP_NOT_FOUND);
        }

        return $response;
    }

    /**
     * get route list from file
     *
     * @return Routing\RouteCollection
     */
    private function getRoutes()
    {
        $locator = new FileLocator(__DIR__ . '/config');
        $loader = new YamlFileLoader($locator);
        return $loader->load('routes.yml');
    }
}