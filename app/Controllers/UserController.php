<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Lib\Parameter;

class UserController
{
    public function showAction()
    {
        return Parameter::get('twitter_users');
    }

    public function storeAction(Request $request)
    {
        $name = $request->request->get('name');
        if (!empty($name)) {
            $users = Parameter::get('twitter_users');
            if (!array_search($name, $users)) {
                $users[] = $name;
                Parameter::set('twitter_users', $users);
                return ['message' => 'User added'];
            } else
                return new JsonResponse(['message' => 'Name already exist'], 400);
        } else
            return new JsonResponse(['message' => 'Name not found'], 400);
    }

    public function deleteAction($name)
    {
        $users = Parameter::get('twitter_users');
        $users = array_diff($users, [$name]);
        Parameter::set('twitter_users', array_values($users));
        return ['message' => "User $name was removed"];
    }
}