<?php

namespace App\Lib;

use Symfony\Component\Yaml\Yaml;

class Parameter
{
    const PARAMETERS = __DIR__ . '/../config/parameters.yml';

    /**
     * Get parameter list from file
     *
     * @return array
     */
    public static function all()
    {
        $parameters = file_get_contents(self::PARAMETERS);
        return Yaml::parse($parameters);
    }

    /**
     * Get parameter from parameter list
     *
     * @param string $parameter
     *
     * @return mixed
     */
    public static function get($parameter)
    {
        $parameters = self::all();
        return $parameters[$parameter];
    }

    /**
     * Write parameter to file
     *
     * @param string $parameter
     * @param mixed $data
     */
    public static function set($parameter, $data)
    {
        $parameters = self::all();
        $parameters[$parameter] = $data;
        $parameters = Yaml::dump($parameters);
        file_put_contents(self::PARAMETERS, $parameters);
    }
}