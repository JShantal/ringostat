<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Lib\Parameter;

class ConfigController
{
    public function showAction()
    {
        return Parameter::get('twitter_app');
    }

    public function updateAction(Request $request)
    {
        $request = $request->request;
        $key = $request->get('key');
        $secret = $request->get('secret');

        if (is_null($key) || is_null($secret) || $key == '' || $secret == "")
            return new JsonResponse(['message' => 'Key or secret is missing'], 400);
        else {
            $config = Parameter::get('twitter_app');
            $config['key'] = $key;
            $config['secret'] = $secret;
            Parameter::set('twitter_app', $config);
            return ['message' => 'Config updated!'];
        }
    }
}