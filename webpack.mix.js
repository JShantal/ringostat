let mix = require('laravel-mix');

mix.setPublicPath('public');
mix.js('resources/assets/js/app.js', 'js')
    .sass('resources/assets/sass/app.scss', 'css');
