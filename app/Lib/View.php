<?php

namespace App\Lib;

use Symfony\Component\HttpFoundation\Response;

class View
{
    const PATH = __DIR__ . '/../../resources/views/';

    /**
     * Return view with response
     *
     * @param $name
     *
     * @return Response
     */
    static function show($name, $data = null)
    {
        ob_start();
        include self::PATH . $name . '.view.php';
        $content = ob_get_contents();
        ob_end_clean();
        return new Response($content);
    }
}